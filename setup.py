import json
from setuptools import setup, find_packages

with open('metainfo.json') as file:
    metainfo = json.load(file)

setup(
    name='domain_of_applicability',
    version='1.0',
    author=', '.join(metainfo['authors']),
    author_email=metainfo['email'],
    url=metainfo['url'],
    description=metainfo['title'],
    long_description=metainfo['description'],
    packages=find_packages(),
    install_requires=['pandas', 'numpy' ],
)


